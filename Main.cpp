#include<iostream>


void FindOddNumbers(int limit, bool isOdd)
{

    for (int i = 0; i <= limit; i++)
    {
    if (i % 2 == 0)
      {
        if (isOdd)
        {
            std::cout << i << " ";
        }
      }
    else
      {
        if (!isOdd)
        {
            std::cout << i << " ";
        }
      }
    }
    std::cout << std::endl;
}

int main()
{
    int limit;
    std::cout << "Enter the limit: ";
    std::cin >> limit;

    std::cout << "Odd numbers up to " << limit << ": ";
    FindOddNumbers(limit, true);

    std::cout << "Even numbers up to " << limit << ": ";
    FindOddNumbers(limit, false);

    return 0;
}